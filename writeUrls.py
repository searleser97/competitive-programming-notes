import os

rootPath = "./src/images/"

listOfFiles = list()
for (dirpath, dirnames, filenames) in os.walk(rootPath):
    listOfFiles += [os.path.join(dirpath, file) for file in filenames]

listOfFiles.sort(key=lambda path: os.stat(path).st_mtime, reverse=True)

output = "const serverURL = \"https://searleser97.gitlab.io/competitive-programming-notes/\";\n\n"
output += "export const imagesurl = [\n"
for f in listOfFiles:
    output += f"{' ' * 2} serverURL + \"{f[len(rootPath):]}\",\n"
output += "];\n"
print(output)
with open("src/constants.tsx", "w+") as f:
    f.write(output)

