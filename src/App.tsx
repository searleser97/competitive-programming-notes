import React from "react";
import "./App.css";
import { imagesurl } from "./constants";
import LazyLoad from "react-lazyload";

const App: React.FC = () => {
  const ScreenshotsList = imagesurl.map((url, idx) => {
    return (
      <div
        key={`${idx}div`}
        style={{
          width: "400px",
          height: "350px",
          border: "1px solid black",
          margin: "1px 1px 1px 1px",
        }}
      >
        <a href={url}>
          <LazyLoad
            key={`${idx}lazyload`}
            overflow={true}
            resize={true}
            throttle={200}
            height={250}
          >
            <img
              key={idx + "_" + url}
              style={{ width: "400px", height: "350px" }}
              src={url}
              alt={url}
            />
          </LazyLoad>
        </a>
      </div>
    );
  });
  return (
    <div
      style={{
        display: "flex",
        height: "100vh",
        overflow: "auto",
        flexWrap: "wrap",
      }}
    >
      {ScreenshotsList}
    </div>
  );
};

export default App;
